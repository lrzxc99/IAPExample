#include <stdio.h>

#include "menu.h"
#include "usart.h"
#include "flash_if.h"

extern UART_HandleTypeDef huart1;

void Main_Menu(void)
{
	
  uint8_t cKey = 0;
	
	pFunction JumpToApplication = NULL;
	uint32_t JumpAddress = 0;
	
  printf("\r\n===========================================================");
  printf("\r\n=              (C) COPYRIGHT(c) 2019 KE1                  =");
  printf("\r\n=                                                         =");
  printf("\r\n=         KE1 In-Application Programming Application      =");
  printf("\r\n=                                                         =");
  printf("\r\n=                    By Hardware Team                     =");
  printf("\r\n===========================================================");
  printf("\r\n\r\n");
	
	printf("\r\n=================== Main Menu =============================\r\n");
	printf("  Download image to the internal Flash ----------------- 1\r\n\n");
	printf("  Execute the loaded application ----------------------- 2\r\n\n");
	do{
		cKey = uart1_get_char();
	}while(0 == cKey);
	
	switch(cKey){
		case '1':
			printf("Download image to the internal Flash\r\n");
			break;
		case '2':
			printf("\r\nExecute the loaded application\r\n");
			__set_PRIMASK(1); 
			if (((*(__IO uint32_t*)APPLICATION_ADDRESS) & 0x2FFE0000 ) == 0x20000000)
			{
				/* Jump to user application */
				JumpAddress = *(__IO uint32_t*) (APPLICATION_ADDRESS + 4);
				JumpToApplication = (pFunction) JumpAddress;
				/* Initialize user application's Stack Pointer */
				__set_MSP(*(__IO uint32_t*) APPLICATION_ADDRESS);
				printf("Jump To Application!\r\n");
				JumpToApplication();
			}
			break;
		default:
			printf("Not supported!\r\n");
			break;
	}
}

