#include <stdio.h>

#include "usart.h"
#include "flash_if.h"

static uint32_t GetPage(uint32_t Addr)
{
  uint32_t page = 0;
  
  if (Addr < (FLASH_BASE + FLASH_BANK_SIZE)){
    /* Bank 1 */
    page = (Addr - FLASH_BASE) / FLASH_PAGE_SIZE;
  }else{
    /* Bank 2 */
    page = (Addr - (FLASH_BASE + FLASH_BANK_SIZE)) / FLASH_PAGE_SIZE;
  }
  return page;
}

/**
  * @brief  Unlocks Flash for write access
  * @param  None
  * @retval None
  */
static void FLASH_If_Init(void)
{ 
  HAL_FLASH_Unlock(); 

  /* Clear pending flags (if any) */  
  __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | 
                         FLASH_FLAG_PGAERR | FLASH_FLAG_PROGERR | FLASH_FLAG_PGSERR);
}

/**
  * @brief  This function does an erase of all user flash area
  * @param  StartSector: start of user flash area
  * @retval 0: user flash area successfully erased
  *         1: error occurred
  */
uint32_t FLASH_If_Erase(uint32_t uiUserStartFlashAddr, uint32_t uiUserEndFlashAddr)
{
	HAL_StatusTypeDef halSta;
  uint32_t FirstPage = 0, NbOfPages = 0, BankNumber = 0, PAGEError = 0;
	
	FLASH_EraseInitTypeDef EraseInitStruct;
  /* Unlock the Flash to enable the flash control register access *************/ 
  FLASH_If_Init();
  
	/* Get the 1st page to erase */
  FirstPage = GetPage(uiUserStartFlashAddr);
  /* Get the number of pages to erase from 1st page */
  NbOfPages = GetPage(uiUserEndFlashAddr) - FirstPage + 1;
  /* Get the bank */
  BankNumber = FLASH_BANK_1;
  /* Fill EraseInit structure*/
  printf("Erase FirstPage:%d NbOfPages:%d\n", FirstPage, NbOfPages);
	
	EraseInitStruct.TypeErase   = FLASH_TYPEERASE_PAGES;
  EraseInitStruct.Banks       = BankNumber;
  EraseInitStruct.Page        = FirstPage;
  EraseInitStruct.NbPages     = NbOfPages;
	
	halSta = HAL_FLASHEx_Erase(&EraseInitStruct, &PAGEError);
  if (HAL_OK != halSta){
    /*
      Error occurred while page erase.
      User can add here some code to deal with this error.
      PAGEError will contain the faulty page and then to know the code error on this page,
      user can call function 'HAL_FLASH_GetError()'
    */
		printf("HAL_FLASHEx_Erase Err:%d PageErr:%d\n", halSta, PAGEError);
    /* Infinite loop */
    while (1)
    {
      HAL_Delay(1000);
    }
  }
  
  return (FLASHIF_OK);
}

uint32_t FLASH_If_Write(uint32_t uiUserStartFlashAddr, uint32_t *puiData ,uint32_t uiDataLength)
{
	uint32_t i = 0;

  for (i = 0; (i < uiDataLength) && (uiUserStartFlashAddr <= (USER_FLASH_END_ADDRESS-4)); i++){
    /* Device voltage range supposed to be [2.7V to 3.6V], the operation will
       be done by word */ 
    if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, uiUserStartFlashAddr, *(uint32_t*)(puiData+i)) == HAL_OK){
     /* Check the written value */
      if (*(uint32_t*)uiUserStartFlashAddr != *(uint32_t*)(puiData+i)){
        /* Flash content doesn't match SRAM content */
        return(FLASHIF_WRITINGCTRL_ERROR);
      }
      /* Increment FLASH destination address */
      uiUserStartFlashAddr += 4;
    }else{
      /* Error occurred while writing data in Flash memory */
      return (FLASHIF_WRITING_ERROR);
    }
  }

  return (FLASHIF_OK);

}

/**
  * @brief  Returns the write protection status of user flash area.
  * @param  None
  * @retval 0: No write protected sectors inside the user flash area
  *         1: Some sectors inside the user flash area are write protected
  */
uint16_t FLASH_If_GetWriteProtectionStatus(uint32_t uiUserStartFlashAddr, uint32_t uiUserEndFlashAddr)
{
	uint32_t StartPage = 0, EndPage = 0;
	
  FLASH_OBProgramInitTypeDef OptionsBytesStruct;

  /* Unlock the Flash to enable the flash control register access *************/ 
  HAL_FLASH_Unlock();

  /* Clear OPTVERR bit set on virgin samples */
  __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_OPTVERR); 

  /* Unlock the Options Bytes *************************************************/
  HAL_FLASH_OB_Unlock();
	
	/* Get the number of the start and end pages */
  StartPage = GetPage(uiUserStartFlashAddr);
  EndPage   = GetPage(uiUserEndFlashAddr);
	
	OptionsBytesStruct.WRPArea  = OB_WRPAREA_BANK1_AREAA;

  /* Get pages write protection status ****************************************/
  HAL_FLASHEx_OBGetConfig(&OptionsBytesStruct);
	
  /* Generate System Reset to load the new option byte values ***************/
  if (OptionsBytesStruct.OptionType == OPTIONBYTE_WRP){
    HAL_FLASH_OB_Launch();
  }

  /* Lock the Options Bytes *************************************************/
  HAL_FLASH_OB_Lock();
	
	/* Lock the Flash to disable the flash control register access (recommended
     to protect the FLASH memory against possible unwanted operation) *********/
  HAL_FLASH_Lock();
	
	if ((OptionsBytesStruct.WRPStartOffset <= StartPage) && (OptionsBytesStruct.WRPEndOffset >= EndPage))
  {
    return FLASHIF_PROTECTION_WRPENABLED;
  }
	return FLASHIF_PROTECTION_NONE;
}

HAL_StatusTypeDef FLASH_If_WriteProtectionConfig(uint32_t uiUserStartFlashAddr, uint32_t uiUserEndFlashAddr)
{
	uint32_t StartPage = 0, EndPage = 0;
	HAL_StatusTypeDef halSta = HAL_OK;
  FLASH_OBProgramInitTypeDef OptionsBytesStruct;

  /* Unlock the Flash to enable the flash control register access *************/ 
  HAL_FLASH_Unlock();

  /* Clear OPTVERR bit set on virgin samples */
  __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_OPTVERR); 

  /* Unlock the Options Bytes *************************************************/
  HAL_FLASH_OB_Unlock();
	
	/* Get the number of the start and end pages */
  StartPage = GetPage(uiUserStartFlashAddr);
  EndPage   = GetPage(uiUserEndFlashAddr);
	
	OptionsBytesStruct.WRPArea  = OB_WRPAREA_BANK1_AREAA;
	OptionsBytesStruct.OptionType     = OPTIONBYTE_WRP;
	OptionsBytesStruct.WRPStartOffset = StartPage;
	OptionsBytesStruct.WRPEndOffset   = EndPage;
	
	halSta= HAL_FLASHEx_OBProgram(&OptionsBytesStruct);
	/* Generate System Reset to load the new option byte values ***************/
  if (OptionsBytesStruct.OptionType == OPTIONBYTE_WRP){
    HAL_FLASH_OB_Launch();
  }

  /* Lock the Options Bytes *************************************************/
  HAL_FLASH_OB_Lock();
	
	/* Lock the Flash to disable the flash control register access (recommended
     to protect the FLASH memory against possible unwanted operation) *********/
  HAL_FLASH_Lock();
	return halSta;
}



