合并烧写：
在烧写程序时，需要先用烧写器烧写IAP程序，就是BOOTLOADER的部分，然后再通过外设写入用户程序。
但其实，这两步可以合并为一步烧写。
把IAP的.hex最后一句结束语去掉，即删除 :00000001FF；然后把用户程序的.hex全部内容复制到IAP的.hex后面；最后把整合后的.hex文件烧写到0x0800 0000的起始地址即可。
