#ifndef _IAP_FLASH_IF_H
#define _IAP_FLASH_IF_H

#include "stm32l4xx_hal.h"

/* Error code */
enum 
{
  FLASHIF_OK = 0,
  FLASHIF_ERASEKO,
  FLASHIF_WRITINGCTRL_ERROR,
  FLASHIF_WRITING_ERROR
};

enum{
  FLASHIF_PROTECTION_NONE         = 0,
  FLASHIF_PROTECTION_PCROPENABLED = 0x1,
  FLASHIF_PROTECTION_WRPENABLED   = 0x2,
  FLASHIF_PROTECTION_RDPENABLED   = 0x4,
};

/* End of the Flash address */
#define USER_FLASH_END_ADDRESS				(uint32_t)0x081E0000 /*0x08005000 -> 0x081E0000 = 100kbytes*/
/* Define the user application size */
#define USER_FLASH_SIZE   (USER_FLASH_END_ADDRESS - APPLICATION_ADDRESS + 1) // 100K

/* Define the address from where user application will be loaded.
   Note: the 1st sector 0x08000000-0x08003FFF is reserved for the IAP code */
#define APPLICATION_ADDRESS   				(uint32_t)0x08004000 

uint32_t FLASH_If_Erase(uint32_t uiUserStartFlashAddr, uint32_t uiUserEndFlashAddr);

uint32_t FLASH_If_Write(uint32_t uiUserStartFlashAddr, uint32_t *puiData ,uint32_t uiDataLength);

uint16_t FLASH_If_GetWriteProtectionStatus(uint32_t uiUserStartFlashAddr, uint32_t uiUserEndFlashAddr);

HAL_StatusTypeDef FLASH_If_WriteProtectionConfig(uint32_t uiUserStartFlashAddr, uint32_t uiUserEndFlashAddr);

#endif
